# Build static contents -> mkdocs
# FROM python:3-alpine
FROM squidfunk/mkdocs-material

# Packages qui seront conservés
RUN set -eux \
 && apk add --no-cache \
      # For update dates
      git \
      # For PDF-EXPORT-PLUGIN
      libffi cairo pango gdk-pixbuf libstdc++ openjpeg

# Liste des dépendances / packages
COPY requirements.txt .

# Installation des composants Python (packages de dev seront retirés)
RUN set -eux \
 && apk add --no-cache --virtual .build-deps \
      tar \
      gzip \
      make \
      gcc g++ \
      libc-dev \
      musl-dev openjpeg-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev \
 && python -m pip install --upgrade pip \
 && python -m pip install -r requirements.txt \
 && apk del --no-cache .build-deps


# Import Fonts (for PDF generator)
#COPY fonts/static /usr/share/fonts/Additional
RUN apk --update --upgrade --no-cache add fontconfig ttf-dejavu \ 
 && fc-cache -f \ 
 && fc-list | sort 
