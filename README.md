# Mkdocs+material ... + PDF

Cette image est une surcharge de l'image mkdocs+material de [**Squidfunk**](https://squidfunk.github.io/mkdocs-material/)

**Image Docker de départ** : squidfunk/mkdocs-material... disponible sur le [**hub.docker.com**](https://hub.docker.com/r/squidfunk/mkdocs-material)

Cette surcharge utilise toujours le tag latest, et réalise un build quotidien.

Composants ajoutés : (voir `requirements.txt`)
- mkdocs-git-revision-date-localized-plugin
- mkdocs-with-pdf
- mike

Pour construire "la même image" depuis l'image de départ `python:latest`, on peut ajouter dans requirements.txt les lignes suivantes :

```
mkdocs>=1.2
Pygments>=2.4
pymdown-extensions>=7.0
mkdocs-material
mkdocs-material-extensions>=1.0
markdown>=3.2
markdown-blockdiag
markdown-include
mike
```

Le résultat sera plus gros et laissera à notre charge le suivi des versions.
